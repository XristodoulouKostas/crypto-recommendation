package com.xm.exercise.cryptorecommendation.interceptors;

import io.github.bucket4j.Bandwidth;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.Refill;
import org.slf4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Component
public class BucketProvider {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(BucketProvider.class);

    private Map<String, Bucket> bucketPerIp;
    private static final int REQUESTS_PER_MINUTE = 20;

    public Bucket getBucketForIp(String ip) {
        if (Objects.isNull(bucketPerIp)) {
            bucketPerIp = new HashMap<>();
        }
        return bucketPerIp.computeIfAbsent(ip, this::createBucket);
    }

    private Bucket createBucket(String ip) {
        Bandwidth limit = Bandwidth.classic(REQUESTS_PER_MINUTE, Refill.intervally(REQUESTS_PER_MINUTE, Duration.ofMinutes(1)));
        return Bucket.builder()
                .addLimit(limit)
                .build();
    }

    // Clear cache every hour
    @Scheduled(cron = "0 0/30 * * * ?")
    private void resetBuckets() {
        if (Objects.nonNull(bucketPerIp)) {
            log.info("Clearing bucket cache");
            bucketPerIp.clear();
        }
    }

}
