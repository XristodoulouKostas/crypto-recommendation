package com.xm.exercise.cryptorecommendation.services;

import com.xm.exercise.cryptorecommendation.models.CryptoCurrency;
import com.xm.exercise.cryptorecommendation.exceptions.NoDataFoundException;
import com.xm.exercise.cryptorecommendation.exceptions.TickerNotSupportedException;
import com.xm.exercise.cryptorecommendation.repositories.CryptoValueRecord;
import com.xm.exercise.cryptorecommendation.repositories.CryptoValueRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class CryptoService {

    private final Set<String> supportedCryptocurrencies;
    private final CryptoValueRecordRepository cryptoRepository;

    public CryptoService(@Value("${crypto.supported}") Set<String> supportedCryptocurrencies, @Autowired CryptoValueRecordRepository cryptoRepository) {
        this.supportedCryptocurrencies = supportedCryptocurrencies;
        this.cryptoRepository = cryptoRepository;
    }

    public List<CryptoCurrency> getCryptoCurrencies(Optional<Integer> periodInMonths) {
        return supportedCryptocurrencies.stream()
                .map(ticker -> getCryptoCurrency(ticker, periodInMonths))
                .sorted(Comparator.comparing(CryptoCurrency::getNormalizedRange, Comparator.reverseOrder()))
                .collect(Collectors.toList());
    }

    public CryptoCurrency getCryptoCurrency(String ticker, Optional<Integer> periodInMonths) {
        if (!supportedCryptocurrencies.contains(ticker)) {
            throw TickerNotSupportedException.ofTicker(ticker);
        }

        List<CryptoValueRecord> records = cryptoRepository.findByTicker(ticker).stream()
                .filter(Objects::nonNull)
                .filter(insideOfPeriod(periodInMonths))
                .toList();

        return fromRecords(ticker, records);
    }

    public CryptoCurrency getCryptoCurrenciesForDate(LocalDate date) {
        Map<String, List<CryptoValueRecord>> cryptoValueRecordsByTicker = cryptoRepository.findByDate(date).stream()
                .collect(Collectors.groupingBy(CryptoValueRecord::getTicker, Collectors.toList()));

        return cryptoValueRecordsByTicker.entrySet().stream()
                .map(entry -> fromRecords(entry.getKey(), entry.getValue()))
                .max(Comparator.comparing(CryptoCurrency::getNormalizedRange, Comparator.naturalOrder()))
                .orElseThrow(() -> new NoDataFoundException("No records were found for " + date));
    }

    private CryptoCurrency fromRecords(String ticker, List<CryptoValueRecord> records) {
        if (records.isEmpty()) {
            throw NoDataFoundException.forTicker(ticker);
        }

        List<CryptoValueRecord> orderedByDateTime = records.stream()
                .sorted(Comparator.comparing(CryptoValueRecord::getDateTime, Comparator.naturalOrder())).toList();
        CryptoValueRecord newestRecord = orderedByDateTime.get(records.size() - 1);
        CryptoValueRecord oldestRecord = orderedByDateTime.get(0);

        BigDecimal min = records.stream()
                .map(CryptoValueRecord::getValue)
                .min(Comparator.naturalOrder())
                .orElse(BigDecimal.ZERO);

        BigDecimal max = records.stream()
                .map(CryptoValueRecord::getValue)
                .max(Comparator.naturalOrder())
                .orElse(BigDecimal.ZERO);

        return CryptoCurrency.builder()
                .withTicker(ticker)
                .withOldestValue(oldestRecord.getValue())
                .withNewestValue(newestRecord.getValue())
                .withMaxValue(max)
                .withMinValue(min)
                .withPeriod(oldestRecord.getDateTime(), newestRecord.getDateTime())
                .build();
    }

    private Predicate<CryptoValueRecord> insideOfPeriod(Optional<Integer> periodInMonths) {
        if (periodInMonths.isPresent()) {
            LocalDateTime startDateTime = LocalDateTime.now().minus(periodInMonths.get(), ChronoUnit.MONTHS);
            return cryptoValueRecord -> cryptoValueRecord.getDateTime().isAfter(startDateTime);
        } else {
            return cryptoValueRecord -> true;
        }
    }

}
