package com.xm.exercise.cryptorecommendation.controllers;

import com.xm.exercise.cryptorecommendation.models.CryptoCurrency;
import com.xm.exercise.cryptorecommendation.services.CryptoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class CryptoControllerTest {

    @Mock
    private CryptoService cryptoService;
    @InjectMocks
    private CryptoController cryptoController;

    @Test
    void testCryptoCurrenciesRetrieval_NoCryptoFound() {
        Mockito.when(cryptoService.getCryptoCurrencies(Optional.empty())).thenReturn(List.of());

        ResponseEntity<List<CryptoCurrency>> response = cryptoController.getCryptoCurrencies(Optional.empty());

        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertNotNull(response.getBody());
        Assertions.assertTrue(response.getBody().isEmpty());
    }

    @Test
    void testCryptoCurrenciesRetrieval_CryptosFound() {
        List<CryptoCurrency> cryptoCurrencies = List.of(
                CryptoCurrency.builder()
                        .withTicker("Ticker")
                        .withMinValue(BigDecimal.ONE)
                        .withMaxValue(BigDecimal.TEN)
                        .withPeriod(LocalDateTime.now(), LocalDateTime.now().plus(1, ChronoUnit.DAYS))
                        .build());

        Mockito.when(cryptoService.getCryptoCurrencies(Optional.empty())).thenReturn(cryptoCurrencies);

        ResponseEntity<List<CryptoCurrency>> response = cryptoController.getCryptoCurrencies(Optional.empty());

        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertNotNull(response.getBody());
        Assertions.assertEquals(cryptoCurrencies, response.getBody());
    }

}
