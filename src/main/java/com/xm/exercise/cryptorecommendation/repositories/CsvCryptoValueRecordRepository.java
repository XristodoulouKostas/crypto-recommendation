package com.xm.exercise.cryptorecommendation.repositories;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class CsvCryptoValueRecordRepository implements CryptoValueRecordRepository {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(CsvCryptoValueRecordRepository.class);

    @Value("${crypto.file-path}")
    private String basePath;
    @Value("${crypto.supported}")
    Set<String> supportedCryptocurrencies;

    @Override
    public List<CryptoValueRecord> findByTicker(String ticker) {
        Path filePath = Path.of(basePath,  ticker + "_values.csv");
        String path = getAbsolutePath(filePath);
        try (Stream<String> fileStream = Files.lines(Path.of(path))) {
            return fileStream
                    .skip(1)
                    .map(CryptoValueRecord::fromCsvLine)
                    .collect(Collectors.toList());
        } catch (IOException ex) {
            log.error("Could not extract results from {}", path);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public List<CryptoValueRecord> findByDate(LocalDate date) {
        return supportedCryptocurrencies.stream()
                .map(this::findByTicker)
                .flatMap(List::stream)
                .filter(cryptoValueRecord -> cryptoValueRecord.getDateTime().toLocalDate().equals(date))
                .collect(Collectors.toList());
    }

    private String getAbsolutePath(Path filePath) {
        try {
            return ResourceUtils.getURL(filePath.toString()).getPath();
        } catch (FileNotFoundException e) {
            log.error("Could not find the file {}", filePath);
            throw new RuntimeException(e);
        }
    }
}
