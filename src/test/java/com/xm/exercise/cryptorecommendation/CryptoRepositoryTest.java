package com.xm.exercise.cryptorecommendation;

import com.xm.exercise.cryptorecommendation.repositories.CryptoValueRecord;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class CryptoRepositoryTest {

    @Test
    void testCryptoReading() {
        LocalDateTime now = LocalDateTime.of(2022, 9, 18, 10, 0);
        String csvLine = (now.atZone(ZoneId.systemDefault()).toInstant().getEpochSecond() * 1000) + ",BTC,46813.21";
        System.out.println(csvLine);
        CryptoValueRecord cryptoValueRecord = CryptoValueRecord.fromCsvLine(csvLine);
        Assertions.assertThat(cryptoValueRecord)
                .hasFieldOrPropertyWithValue("dateTime", now)
                .hasFieldOrPropertyWithValue("ticker", "BTC")
                .hasFieldOrPropertyWithValue("value", new BigDecimal("46813.21"));
    }

}
