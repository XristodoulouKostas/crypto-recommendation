package com.xm.exercise.cryptorecommendation.interceptors;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class RateLimitInterceptor implements HandlerInterceptor {

    private final BucketProvider bucketProvider;

    public RateLimitInterceptor(BucketProvider bucketProvider) {
        this.bucketProvider = bucketProvider;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        if (bucketProvider.getBucketForIp(request.getRemoteAddr()).tryConsume(1)) {
            return true;
        } else {
            response.sendError(429, "Too many requests");
            return false;
        }
    }
}