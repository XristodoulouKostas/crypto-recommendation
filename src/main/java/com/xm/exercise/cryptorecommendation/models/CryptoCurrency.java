package com.xm.exercise.cryptorecommendation.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.xm.exercise.cryptorecommendation.utils.Utils;
import org.springframework.lang.NonNull;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CryptoCurrency {

    @NonNull
    private final String ticker;
    private final BigDecimal normalizedRange;
    private final BigDecimal oldestValue;
    private final BigDecimal newestValue;
    private final BigDecimal maxValue;
    private final BigDecimal minValue;
    private final Period period;

    private CryptoCurrency(String ticker, BigDecimal oldestValue, BigDecimal newestValue, BigDecimal maxValue, BigDecimal minValue, Period period) {
        this.ticker = ticker;
        this.oldestValue = oldestValue;
        this.newestValue = newestValue;
        this.maxValue = maxValue;
        this.minValue = minValue;
        this.normalizedRange = Utils.getNormalizedRate(minValue, maxValue);
        this.period = period;
    }

    public static Builder builder() {
        return new Builder();
    }

    @NonNull
    public String getTicker() {
        return ticker;
    }

    public BigDecimal getNormalizedRange() {
        return normalizedRange;
    }

    public BigDecimal getOldestValue() {
        return oldestValue;
    }

    public BigDecimal getNewestValue() {
        return newestValue;
    }

    public BigDecimal getMaxValue() {
        return maxValue;
    }

    public BigDecimal getMinValue() {
        return minValue;
    }

    public Period getPeriod() {
        return period;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CryptoCurrency that = (CryptoCurrency) o;
        return ticker.equals(that.ticker) && Objects.equals(normalizedRange, that.normalizedRange) && Objects.equals(oldestValue, that.oldestValue) && Objects.equals(newestValue, that.newestValue) && Objects.equals(maxValue, that.maxValue) && Objects.equals(minValue, that.minValue) && Objects.equals(period, that.period);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ticker, normalizedRange, oldestValue, newestValue, maxValue, minValue, period);
    }

    @Override
    public String toString() {
        return "CryptoCurrency{" +
                "ticker='" + ticker + '\'' +
                ", normalizedRange=" + normalizedRange +
                ", oldestValue=" + oldestValue +
                ", newestValue=" + newestValue +
                ", maxValue=" + maxValue +
                ", minValue=" + minValue +
                ", period=" + period +
                '}';
    }

    public static class Builder {
        private String ticker;
        private BigDecimal oldestValue = BigDecimal.ZERO;
        private BigDecimal newestValue = BigDecimal.ZERO;
        private BigDecimal maxValue = BigDecimal.ZERO;
        private BigDecimal minValue = BigDecimal.ZERO;
        private Period period;

        public Builder withTicker(String val) {
            this.ticker = val;
            return this;
        }

        public Builder withOldestValue(BigDecimal val) {
            this.oldestValue = val;
            return this;
        }

        public Builder withNewestValue(BigDecimal val) {
            this.newestValue = val;
            return this;
        }

        public Builder withMaxValue(BigDecimal val) {
            this.maxValue = val;
            return this;
        }

        public Builder withMinValue(BigDecimal val) {
            this.minValue = val;
            return this;
        }

        public Builder withPeriod(LocalDateTime start, LocalDateTime end) {
            this.period = new Period(start, end);
            return this;
        }

        public CryptoCurrency build() {
            return new CryptoCurrency(ticker, oldestValue, newestValue, maxValue, minValue, period);
        }
    }
}
