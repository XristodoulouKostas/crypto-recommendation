package com.xm.exercise.cryptorecommendation.services;

import com.xm.exercise.cryptorecommendation.models.CryptoCurrency;
import com.xm.exercise.cryptorecommendation.exceptions.NoDataFoundException;
import com.xm.exercise.cryptorecommendation.exceptions.TickerNotSupportedException;
import com.xm.exercise.cryptorecommendation.repositories.CryptoValueRecord;
import com.xm.exercise.cryptorecommendation.repositories.CryptoValueRecordRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@ExtendWith(MockitoExtension.class)
public class CryptoServiceTest {

    private static final Set<String> SUPPORTED_CRYPTOS = Set.of("BTC", "ETH");
    private final CryptoValueRecordRepository cryptoValueRecordRepository = Mockito.mock(CryptoValueRecordRepository.class);
    private CryptoService cryptoService;

    @BeforeEach
    public void setUp() {
        cryptoService = new CryptoService(SUPPORTED_CRYPTOS, cryptoValueRecordRepository);
    }

    @Test
    void testGetCryptocurrency_TickerNotSupported() {
        Assertions.assertThatThrownBy(() -> cryptoService.getCryptoCurrency("NOT", Optional.empty()))
                .isInstanceOf(TickerNotSupportedException.class);
    }

    @Test
    void testGetCryptocurrency_NoDataFound() {
        final String ticker = "BTC";
        Mockito.when(cryptoValueRecordRepository.findByTicker(ticker)).thenReturn(List.of());
        Assertions.assertThatThrownBy(() -> cryptoService.getCryptoCurrency(ticker, Optional.empty()))
                .isInstanceOf(NoDataFoundException.class);
    }

    @Test
    void testGetCryptocurrency_NoPeriodSpecified() {
        final String ticker = "BTC";
        LocalDateTime now = LocalDateTime.now().minus(10, ChronoUnit.MINUTES);
        Mockito.when(cryptoValueRecordRepository.findByTicker(ticker)).thenReturn(getTestData(ticker, now));

        CryptoCurrency cryptoCurrency = cryptoService.getCryptoCurrency(ticker, Optional.empty());

        Assertions.assertThat(cryptoCurrency)
                .hasFieldOrPropertyWithValue("ticker", ticker)
                .hasFieldOrPropertyWithValue("normalizedRange", BigDecimal.valueOf(14))
                .hasFieldOrPropertyWithValue("oldestValue", BigDecimal.valueOf(10))
                .hasFieldOrPropertyWithValue("newestValue", BigDecimal.valueOf(100))
                .hasFieldOrPropertyWithValue("maxValue", BigDecimal.valueOf(150))
                .hasFieldOrPropertyWithValue("minValue", BigDecimal.valueOf(10));
    }

    @Test
    void testGetCryptocurrency_Last2Months() {
        final String ticker = "BTC";
        LocalDateTime now = LocalDateTime.now().minus(10, ChronoUnit.MINUTES);
        Mockito.when(cryptoValueRecordRepository.findByTicker(ticker)).thenReturn(getTestData(ticker, now));

        CryptoCurrency cryptoCurrency = cryptoService.getCryptoCurrency(ticker, Optional.of(2));

        Assertions.assertThat(cryptoCurrency)
                .hasFieldOrPropertyWithValue("ticker", ticker)
                .hasFieldOrPropertyWithValue("normalizedRange", BigDecimal.valueOf(1))
                .hasFieldOrPropertyWithValue("oldestValue", BigDecimal.valueOf(50))
                .hasFieldOrPropertyWithValue("newestValue", BigDecimal.valueOf(100))
                .hasFieldOrPropertyWithValue("maxValue", BigDecimal.valueOf(100))
                .hasFieldOrPropertyWithValue("minValue", BigDecimal.valueOf(50));
    }

    @Test
    void testGetCryptocurrencies_Last2Months() {
        LocalDateTime now = LocalDateTime.now().minus(10, ChronoUnit.MINUTES);
        Mockito.when(cryptoValueRecordRepository.findByTicker("BTC")).thenReturn(getTestData("BTC", now));
        Mockito.when(cryptoValueRecordRepository.findByTicker("ETH")).thenReturn(getTestData("ETH", now));

        List<CryptoCurrency> cryptoCurrencies = cryptoService.getCryptoCurrencies(Optional.of(2));

        Assertions.assertThat(cryptoCurrencies)
                .isNotEmpty()
                .hasSize(2)
                .contains(
                        CryptoCurrency.builder()
                                .withTicker("BTC")
                                .withOldestValue(BigDecimal.valueOf(50))
                                .withNewestValue(BigDecimal.valueOf(100))
                                .withMaxValue(BigDecimal.valueOf(100))
                                .withMinValue(BigDecimal.valueOf(50))
                                .withPeriod(now.minus(1, ChronoUnit.MONTHS), now)
                                .build(),
                        CryptoCurrency.builder()
                                .withTicker("ETH")
                                .withOldestValue(BigDecimal.valueOf(50))
                                .withNewestValue(BigDecimal.valueOf(100))
                                .withMaxValue(BigDecimal.valueOf(100))
                                .withMinValue(BigDecimal.valueOf(50))
                                .withPeriod(now.minus(1, ChronoUnit.MONTHS), now)
                                .build()
                );
    }

    private List<CryptoValueRecord> getTestData(String ticker, LocalDateTime now) {
        return List.of(
                new CryptoValueRecord(now, ticker, BigDecimal.valueOf(100)),
                new CryptoValueRecord(now.minus(1, ChronoUnit.MONTHS), ticker, BigDecimal.valueOf(50)),
                new CryptoValueRecord(now.minus(2, ChronoUnit.MONTHS), ticker, BigDecimal.valueOf(150)),
                new CryptoValueRecord(now.minus(3, ChronoUnit.MONTHS), ticker, BigDecimal.valueOf(10))
        );
    }


}
