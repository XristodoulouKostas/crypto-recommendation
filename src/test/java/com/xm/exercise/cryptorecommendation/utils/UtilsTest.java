package com.xm.exercise.cryptorecommendation.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.math.BigDecimal;

public class UtilsTest {

    @Test
    void testGetNormalizedRate_minValueNull() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                Utils.getNormalizedRate(null, BigDecimal.ONE)
        );
    }

    @Test
    void testGetNormalizedRate_minValueZero() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                Utils.getNormalizedRate(BigDecimal.ZERO, BigDecimal.ONE)
        );
    }

    @Test
    void testGetNormalizedRate_maxValueNull() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                Utils.getNormalizedRate(BigDecimal.ONE, null)
        );
    }

    @ParameterizedTest
    @CsvSource({
            "2, 4, 1",
            "1, 4, 3",
            "5, 25, 4"
    })
    void testGetNormalizedRate(Integer min, Integer max, Integer expected) {
        BigDecimal result = Utils.getNormalizedRate(BigDecimal.valueOf(min), BigDecimal.valueOf(max));
        Assertions.assertEquals(BigDecimal.valueOf(expected), result);
    }

}
