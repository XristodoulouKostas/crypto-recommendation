package com.xm.exercise.cryptorecommendation.exceptions;

public class NoDataFoundException extends RuntimeException {

    public NoDataFoundException() {
    }

    public NoDataFoundException(String message) {
        super(message);
    }

    public static NoDataFoundException forTicker(String ticker) {
        return new NoDataFoundException("No data were found for cryptocurrency " + ticker);
    }

}
