package com.xm.exercise.cryptorecommendation.exceptions;

public class TickerNotSupportedException extends RuntimeException {

    public static TickerNotSupportedException ofTicker(String ticker) {
        return new TickerNotSupportedException("Cryptocurrency " + ticker + " is not currently supported");
    }

    public TickerNotSupportedException() {
    }

    public TickerNotSupportedException(String message) {
        super(message);
    }
}
