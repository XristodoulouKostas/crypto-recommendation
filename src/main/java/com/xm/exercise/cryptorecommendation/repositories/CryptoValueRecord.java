package com.xm.exercise.cryptorecommendation.repositories;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class CryptoValueRecord {
    private final LocalDateTime dateTime;
    private final String ticker;
    private final BigDecimal value;

    public CryptoValueRecord(LocalDateTime dateTime, String ticker, BigDecimal value) {
        this.dateTime = dateTime;
        this.ticker = ticker;
        this.value = value;
    }

    public static CryptoValueRecord fromCsvLine(String csvLine) {
        String[] data = csvLine.split(",", 3);
        final LocalDateTime dateTime = Instant.ofEpochMilli(Long.parseLong(data[0].trim())).atZone(ZoneId.systemDefault()).toLocalDateTime();
        return new CryptoValueRecord(dateTime, data[1].trim(), new BigDecimal(data[2].trim()));
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public String getTicker() {
        return ticker;
    }

    public BigDecimal getValue() {
        return value;
    }
}
