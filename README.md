# Crypto Recommendation Service

Crypto Recommendation service is implemented using Java 17 and Spring Boot framework.

### Data Storage
The data regarding the cryptocurrencies values are placed in the file system, in the folder _/prices_. We could easily
switch and fetch data from a different storage (e.g. DB), by creating a new class that implements
CryptoValueRecordRepository and injecting
this one to the service. The rest of the implementation should remain untouched.

### Adding new cryptocurrencies
We can add a new cryptocurrency by adding a new file with the cryptocurrency's data, and make it available by adding its
ticker in the properties file (key: crypto.supported).

Generally we can modify the supported cryptocurrencies by editing the application yaml (For simplicity are kept in the
properties files).

### Selecting a range for the data that will be retrieved and analyzed
The APIs accept an optional query parameter that allows the user to extract data in a specified period. Specifically, 
the user can insert a number of months and retrieve data on these last months (Period: Last X months -> NOW)

### Rate limiting access to the APIs
Access to the APIs is limited to 20 requests/minute for each IP (The number is set for testing purposes).
In order to implement the rate limiter easier, _bucket4j_ library has been used. The number of remaining request
per IP is stored in an in-memory data structure (for faster implementation) which is cleared every half
an hour.

## API Documentation

Once the application is running, you can access the API Documentation (OpenAPI definition) through the following link:

* [Crypto Recommendation service - API Documentation](http://localhost:8080/crypto-recommendation/swagger-ui/index.html)

## Automated testing

Both unit and functional tests are created, in order to test the application, and can be found in the _/src/test_
folder.
Junit and Mockito have been used for the tests, and Assertj library is used to make easier assertions.

Due to limited time, there are some components that are not completely tested. For example the functionality that
retrieves the crypto with the highest normalized rate on a specific date contains much fewer tests than it is supposed
to have.

## Build and run the application on a docker container
You can create a running container of the application by following the steps below:

1. _Create executable_: "mvn clean install"
2. _Build image_: "docker build -t crypto-app ."
3. _Run image_: "docker run -p 8080:8080 --name crypto-recommendation-container crypto-app"

You now can access the app using the following links:

* [API Documentation](http://localhost:8080/crypto-recommendation/swagger-ui/index.html)
* [Get Cryptos](http://localhost:8080/crypto-recommendation/cryptos)
* [Get Cryptos - last 12 months](http://localhost:8080/crypto-recommendation/cryptos?months=12)
* [Get specific Crypto](http://localhost:8080/crypto-recommendation/cryptos/BTC)
* [Get specific Crypto - last 12 months](http://localhost:8080/crypto-recommendation/cryptos/BTC?months=12)
* [Get currency with highest normalized rate for a specific date](http://localhost:8080/crypto-recommendation/cryptos/date/2022-01-06)