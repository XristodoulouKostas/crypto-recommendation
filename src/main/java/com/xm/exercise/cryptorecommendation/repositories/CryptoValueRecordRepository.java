package com.xm.exercise.cryptorecommendation.repositories;

import java.time.LocalDate;
import java.util.List;

public interface CryptoValueRecordRepository {

    List<CryptoValueRecord> findByTicker(String ticker);

    List<CryptoValueRecord> findByDate(LocalDate date);

}
