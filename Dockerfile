FROM openjdk:17-jdk-slim-buster

ENTRYPOINT java -jar app.jar

ADD prices /prices

ADD target/crypto-recommendation-service.jar /app.jar
