package com.xm.exercise.cryptorecommendation.functional;

import com.xm.exercise.cryptorecommendation.controllers.CryptoController;
import com.xm.exercise.cryptorecommendation.exceptions.TickerNotSupportedException;
import com.xm.exercise.cryptorecommendation.models.CryptoCurrency;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class CryptoApplicationTest {

    @Autowired
    private CryptoController cryptoController;

    @Test
    void testGetCryptoCurrencies() {
        List<CryptoCurrency> cryptoCurrencies = cryptoController.getCryptoCurrencies(Optional.empty()).getBody();

        Assertions.assertThat(cryptoCurrencies)
                .isNotEmpty()
                .hasSize(5);
    }

    @Test
    void testGetCryptoCurrency_CryptoExists() {
        CryptoCurrency cryptoCurrency = cryptoController.getCryptoCurrencyByTicker("BTC", Optional.empty()).getBody();

        Assertions.assertThat(cryptoCurrency)
                .isNotNull()
                .hasFieldOrPropertyWithValue("ticker", "BTC")
                .hasFieldOrProperty("normalizedRange");
    }

    @Test
    void testGetCryptoCurrency_CryptoDoesNotExists() {
        Assertions.assertThatThrownBy(() -> cryptoController.getCryptoCurrencyByTicker("ABC", Optional.empty()))
                .isInstanceOf(TickerNotSupportedException.class);
    }

    @Test
    void testGetCryptoCurrency_SpecificDate() {
        CryptoCurrency cryptoCurrency = cryptoController.getCryptoCurrencyWithHighestNRByDate(LocalDate.of(2022, 1, 14)).getBody();

        Assertions.assertThat(cryptoCurrency)
                .isNotNull()
                .hasFieldOrProperty("ticker")
                .hasFieldOrProperty("normalizedRange");

        Assertions.assertThat(cryptoCurrency.getPeriod().getStart())
                .hasYear(2022)
                .hasMonth(Month.JANUARY)
                .hasDayOfMonth(14);
        Assertions.assertThat(cryptoCurrency.getPeriod().getEnd())
                .hasYear(2022)
                .hasMonth(Month.JANUARY)
                .hasDayOfMonth(14);
    }

}
