package com.xm.exercise.cryptorecommendation.utils;

import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Utils {

    private Utils() {
    }

    public static BigDecimal getNormalizedRate(BigDecimal min, BigDecimal max) {
        Assert.noNullElements(new Object[]{min, max}, "Min or Max values cannot be null");
        Assert.isTrue(!BigDecimal.ZERO.equals(min), "Minimum value cannot be zero");
        return max.subtract(min).divide(min, RoundingMode.HALF_UP);
    }
}
