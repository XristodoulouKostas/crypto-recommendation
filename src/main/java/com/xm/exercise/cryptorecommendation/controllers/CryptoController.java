package com.xm.exercise.cryptorecommendation.controllers;

import com.xm.exercise.cryptorecommendation.exceptions.NoDataFoundException;
import com.xm.exercise.cryptorecommendation.exceptions.TickerNotSupportedException;
import com.xm.exercise.cryptorecommendation.models.CryptoCurrency;
import com.xm.exercise.cryptorecommendation.services.CryptoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("cryptos")
public class CryptoController {
    private final CryptoService cryptoService;

    public CryptoController(CryptoService cryptoService) {
        this.cryptoService = cryptoService;
    }

    @Operation(summary = "Retrieve information on all supported cryptocurrencies")
    @GetMapping
    public ResponseEntity<List<CryptoCurrency>> getCryptoCurrencies(
            @RequestParam("months") Optional<Integer> periodInMonths
    ) {
        return ResponseEntity.ok(cryptoService.getCryptoCurrencies(periodInMonths));
    }

    @Operation(summary = "Retrieve information on a specific supported cryptocurrency")
    @GetMapping("/{ticker}")
    public ResponseEntity<CryptoCurrency> getCryptoCurrencyByTicker(
            @PathVariable("ticker") String ticker,
            @RequestParam("months") Optional<Integer> periodInMonths
    ) throws TickerNotSupportedException {
        return ResponseEntity.ok(cryptoService.getCryptoCurrency(ticker, periodInMonths));
    }

    @Operation(summary = "Retrieves the cryptocurrency with the highest normalized rate on a specific date")
    @GetMapping("/date/{date}")
    public ResponseEntity<CryptoCurrency> getCryptoCurrencyWithHighestNRByDate(
            @Parameter(description = "Date in yyyy-MM-dd format")
            @PathVariable("date") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date
    ) {
        return ResponseEntity.ok(cryptoService.getCryptoCurrenciesForDate(date));
    }

    @ExceptionHandler(TickerNotSupportedException.class)
    public ResponseEntity<String> handleTickerNotSupportedException(TickerNotSupportedException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    @ExceptionHandler(NoDataFoundException.class)
    public ResponseEntity<String> handleNoDataFoundException(NoDataFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
    }

}

